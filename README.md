# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%      | Y         |


---

### How to use 

    First of all, my canvas is located at the left two thirds part of the window, and you will be holding brush.

    Second, my tools are divided into three parts:
        * Common tools(Brush, Eraser...)
        * Reset, Undo, Redo
        * A pop-out list that have some adjusting tools(Brush size, color...)
    
    To use common tools, you just need to click the specific, then draw something on the canvas with the cursor icon representing the current using tool.
        * Brush: Like common canvas, use mouse to draw.
        * Eraser: Similar to Brush, except that it erases things out.
        * Upload: Click the button, and a window will pop out, letting user
                  to upload some img.
        * Save: Click the button to download the current canvas. The type is png.
        * Text: After clicking the button, user can then click on any position
                on the canvas, and then input blank will pop out for input text.
                Typing text pressing enter, the text will be draw on the canvas.

    On the other hand, when Reset, Undo and Redo button clicked, their functions will show on canvas immediately.

    For the adjusting tools, the details are not originally showed. You have to click the buttons and the tools will pop out, and then you can adjust something like brush size, color etc. Clicking the buttons when the tools are in pop-out mode, then they will fold in, back to the original looking.
        * Brush Size: A slider that can be used to adjust the brush size.
        * Brush colors: Click the color area and then a chart will pop out, then
                        user can choose a color by both typing code of colors or 
                        clicking the desired color.
        * Shapes: Click and six buttons will pop out, respectively for three
                  three filled shapes and hollow shapes.
        * Text Style: Click and the Font list and the Size input can make 
                      customized text style.
                      P.S. the text size ranges from 5 to 80.

### Function description

    I have an extra function that can draw hollow shapes, which can be used by the same way as basic function that draw filled shapes.

### Gitlab page link

    your web page URL, which should be "https://108070038.gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    TAs 辛苦了!

<style>
table th{
    width: 100%;
}
</style>