// Brush(ok), Eraser(ok), Colors(ok), Shapes(X), Up/Download(ok), Un/Redo(ok), Reset(ok), Text(ok)

var canvas;
var ctx;

var typing = false;
var drawing = false;
var drawing_shapes = 0;
var shapeStroke = false;

var lastX, lastY;
var preMoveX, preMoveY;

//var rainbow = false;
//var rainbow_i = 0;
var slider;
var newText;

var nowCanvas;
var nowCanvasImg = new Image();
var step = -1;
var stepArray = [];

var B_or_E = true; // Brush or Eraser

window.onload = function(){
    canvas = document.getElementById('myCanvas');
    ctx = canvas.getContext('2d');
    ctx.lineJoin = "round";
    ctx.lineCap = "round";

    slider = document.getElementById("myRange");
    ctx.lineWidth = slider.value;
    push();
}

// mouse event
function mDown(){

    if(newText) newText.remove();

    if(!B_or_E) ctx.globalCompositeOperation = "destination-out";
    else if(B_or_E) ctx.globalCompositeOperation = "source-over";

    lastX = preMoveX = event.offsetX;
    lastY = preMoveY = event.offsetY;

    ctx.strokeStyle = document.getElementById("color").value;
    ctx.fillStyle = document.getElementById("color").value;

    if(typing){
        let font = document.getElementById("font_list").value;
        let font_size = document.getElementById("font_size").value;
        ctx.font = font_size + "px " + font; //"16px Arial";

        newText = document.createElement("input");
        newText.type = "text";
        newText.style.position = "absolute";
        newText.style.top = lastY+"px";
        newText.style.left = lastX+"px";
        if(!newText.autofocus) newText.autofocus = true;

        document.body.appendChild(newText);
        
        newText.onchange = function(){
            ctx.fillText(this.value, lastX, lastY);
            push();
            this.remove();
        }
    }
    else if(drawing_shapes){
        drawing = true;
    }
    else{
        drawing = true;
        ctx.moveTo(event.offsetX, event.offsetY);
        //ctx.beginPath();
    }
    
}

function mMoving(){
    
    if(typing) return;

    if(drawing){     
        if(!drawing_shapes){
            ctx.beginPath();
            ctx.moveTo(preMoveX, preMoveY);
            ctx.lineTo(event.offsetX, event.offsetY);
            ctx.stroke();
        }
        else if(drawing_shapes == 1){
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(nowCanvasImg, 0, 0);
            ctx.beginPath();
            ctx.rect(lastX, lastY, event.offsetX-lastX, event.offsetY-lastY);
            if(!shapeStroke) ctx.fill();
            else ctx.stroke();
        }
        else if(drawing_shapes == 2){
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(nowCanvasImg, 0, 0);
            ctx.beginPath();
            ctx.ellipse(event.offsetX, event.offsetY, Math.abs(event.offsetX-lastX), Math.abs(event.offsetY-lastY), 0, -Math.PI*1, Math.PI*1, false);
            if(!shapeStroke) ctx.fill();
            else ctx.stroke();
        }
        else if(drawing_shapes == 3){
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(nowCanvasImg, 0, 0);
            ctx.beginPath();
            ctx.moveTo(lastX, lastY);
            ctx.lineTo(lastX-(event.offsetX-lastX), event.offsetY);
            ctx.lineTo(event.offsetX, event.offsetY);
            if(!shapeStroke) ctx.fill();
            else {
                ctx.lineTo(lastX, lastY);
                ctx.stroke();
            }
        }
        preMoveX = event.offsetX;
        preMoveY = event.offsetY;
    }
}

function mUp(){
    if(typing) return;
    drawing = false;
    ctx.closePath();
    push();
}

// click brush icon
function usingBrush(){
    B_or_E = true;
    drawing_shapes = 0;
    typing = false;
    // change icon
    canvas.style.cursor = "url(photo/brush_icon.png) 0 11, auto";
}

// click eraser icon
function usingEraser(){
    B_or_E = false;
    drawing_shapes = 0;
    typing = false;
    // change icon
    //$("#myCanvas").css("cursor", "url(photo/eraser_icon.png) 6 14, auto");
    canvas.style.cursor = "url(photo/eraser_icon.png) 6 14, auto";

}

// reset
function canvasClear(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    push();
}

// slider for brush size
function slide(){
    ctx.lineWidth = slider.value;
}

// undo/redo
function push(){
    step++;
    console.log("undo, step = " + step + ", length = " + stepArray.length);
    if(step+1 < stepArray.length){
        stepArray.length = step+1;
    }
    stepArray.splice(step);
    nowCanvas = canvas.toDataURL();
    nowCanvasImg.src = nowCanvas;
    stepArray.push(nowCanvas);
}

function undo(){
    let flag = 0;
    if(ctx.globalCompositeOperation != "source-over"){
        ctx.globalCompositeOperation = "source-over";
        flag = 1;
    }
    if(step > 0){
        step--;
        console.log("undo, step = " + step + ", length = " + stepArray.length);
        let preCanvas = new Image();
        preCanvas.src = stepArray[step];
        preCanvas.onload = function(){
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(preCanvas, 0, 0);
        }
    }
}

function redo(){
    if(stepArray[step+1] == null) return;
    let flag = 0;
    if(ctx.globalCompositeOperation != "source-over"){
        ctx.globalCompositeOperation = "source-over";
        flag = 1;
    }
    if(step >= 0){
        step++;
        console.log("undo, step = " + step + ", length = " + stepArray.length);
        let nextCanvas = new Image();
        nextCanvas.src = stepArray[step];
        nextCanvas.onload = function(){
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(nextCanvas, 0, 0);
        }
    }
}

// save the canvas
function canvasSave(tar){
    const url = canvas.toDataURL("image/png");
    tar.href = url;
}

// upload image
function upload(tar){
    var img = new Image();
    img.onload = function () {
        this.width = canvas.width;
        this.height = canvas.height;
        if(!B_or_E){
            ctx.globalCompositeOperation = "source-over";
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
            URL.revokeObjectURL(src);
            ctx.globalCompositeOperation = "destination-out";
        }
        else{
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
            URL.revokeObjectURL(src);
        }
        push(); // why not outside ??
    }

    img.onerror = function (){
        console.log("load failed");
    }
    var file = tar.files[0];
    var src = URL.createObjectURL(file);
    img.src = src;
}

// shapes
function drawRect(m){
    shapeStroke = m;
    typing = false;
    drawing_shapes = 1;
    if(!B_or_E) B_or_E = true;
    canvas.style.cursor = "crosshair";
}

function drawCircle(m){
    shapeStroke = m;
    typing = false;
    drawing_shapes = 2;
    if(!B_or_E) B_or_E = true;
    canvas.style.cursor = "crosshair";
    //$("#myCanvas").css("cursor", "crosshair");
}
function drawTri(m){
    shapeStroke = m;
    typing = false;
    drawing_shapes = 3;
    if(!B_or_E) B_or_E = true;
    canvas.style.cursor = "crosshair";
    //$("#myCanvas").css("cursor", "crosshair");
}

function typeText(){
    typing = true;
    B_or_E = true;
    canvas.style.cursor = "url(photo/text_icon.png) 6 3, auto";
}